#!/usr/bin/env bash

# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

if [[ "$#" -ne 4 ]]
then
    echo "Incorrect number of arguments"
    echo "Usage: $0 <properties-file-path> <region> <aws-profile> <stack-name>"
    exit 1
fi

errorExit()
{
    echo $1
    exit 1
}

callCloudFormation()
{
    aws cloudformation $1 \
        --stack-name ${stack_name} \
        --template-body 'file://gitlab-runner.yaml' \
        --disable-rollback  \
        --capabilities CAPABILITY_NAMED_IAM \
        --profile ${profile} \
        --region ${region} \
        --parameters \
            ParameterKey=VpcID,ParameterValue=${VpcID} \
            ParameterKey=SubnetIds,ParameterValue=\"${SubnetIds}\" \
            ParameterKey=ImageId,ParameterValue=${ImageId} \
            ParameterKey=InstanceType,ParameterValue=${InstanceType} \
            ParameterKey=InstanceName,ParameterValue=${InstanceName} \
            ParameterKey=VolumeSize,ParameterValue=${VolumeSize} \
            ParameterKey=VolumeType,ParameterValue=${VolumeType} \
            ParameterKey=MaxSize,ParameterValue=${MaxSize} \
            ParameterKey=MinSize,ParameterValue=${MinSize} \
            ParameterKey=DesiredCapacity,ParameterValue=${DesiredCapacity} \
            ParameterKey=MaxBatchSize,ParameterValue=${MaxBatchSize} \
            ParameterKey=MinInstancesInService,ParameterValue=${MinInstancesInService} \
            ParameterKey=MaxInstanceLifetime,ParameterValue=${MaxInstanceLifetime} \
            ParameterKey=GitlabServerURL,ParameterValue=${GitlabServerURL} \
            ParameterKey=DockerImagePath,ParameterValue=${DockerImagePath} \
            ParameterKey=RunnerRegistrationTokens,ParameterValue=${RunnerRegistrationTokens} \
            ParameterKey=RunnerVersion,ParameterValue=${RunnerVersion} \
            ParameterKey=RunnerEnvironment,ParameterValue=${RunnerEnvironment} \
            ParameterKey=LambdaS3Bucket,ParameterValue=${LambdaS3Bucket} \
            ParameterKey=TimeStamp,ParameterValue=$2 \
            ParameterKey=Concurrent,ParameterValue=${Concurrent} \
            ParameterKey=CheckInterval,ParameterValue=${CheckInterval}
}

verifyStackSuccess()
{
    status=$(aws cloudformation describe-stacks \
                                    --stack-name ${stack_name}  \
                                    --query "Stacks[0].StackStatus" \
                                    --profile ${profile} \
                                    --region ${region} \
                                    --output text) \
                                    || errorExit "Error getting stack status" 2> /dev/null

    if [ "${status}" == "$1" ]; then
        echo "Stack $2 was successful"
    else
        errorExit "Stack $2 failed (status ${status}). See CloudFormation console for error details"
    fi
}

uploadLifecycleHookToS3()
{
  file_name="${stack_name}-lifecycle-hook-${time_stamp}.zip"
  case "$OSTYPE" in
  solaris*) echo "SOLARIS" ;;
  darwin*)  echo "OSX" & zip ${file_name} gitlab-runner-lifecycle-hook.py;;
  linux*)   echo "LINUX" & zip ${file_name} gitlab-runner-lifecycle-hook.py;;
  bsd*)     echo "BSD" ;;
  msys|cygwin*)    echo "WINDOWS" & set MSYS_NO_PATHCONV=1 & jar -cvf ${file_name} gitlab-runner-lifecycle-hook.py;;
  *)        echo "unknown: $OSTYPE" ;;
  esac
  
  aws s3 cp --profile ${profile} ${file_name} s3://${LambdaS3Bucket}/${stack_name}/${file_name}
  if [ $? -ne 0 ]; then
      errorExit "Uploading lifecycle-hook lambda function code to S3 did not complete successfully."
  fi
  rm -f ${file_name}
}

uploadRunnerMonitorToS3() {
  # Note: Current lambda runtime for nodejs14 only supports aws-sdk 2.888.0,
  #  this project uses 3.x, so we have to include the aws-sdk in our function
    
  file_name="${stack_name}-runner-monitor-${time_stamp}.zip"

  cd runner-monitor-lambda
  npm install
  npm run build
  cd bin

  case "$OSTYPE" in
  solaris*) echo "SOLARIS" ;;
  darwin*)  echo "OSX" & zip -r ../../${file_name} *;;
  linux*)   echo "LINUX" & zip -r ../../${file_name} *;;
  bsd*)     echo "BSD" ;;
  msys|cygwin*)    echo "WINDOWS" & set MSYS_NO_PATHCONV=1 & jar -cvf ../../${file_name} *;;
  *)        echo "unknown: $OSTYPE" ;;
  esac

  cd ../..

  aws s3 cp --profile ${profile} ${file_name} s3://${LambdaS3Bucket}/${stack_name}/${file_name}
  if [ $? -ne 0 ]; then
      errorExit "Uploading lifecycle-hook lambda function code to S3 did not complete successfully."
  fi
  rm -f ${file_name}
}

deployStack()
{
    echo -n "Checking if stack already exists..."
    aws cloudformation describe-stacks --stack-name ${stack_name} --profile ${profile} --region ${region} --no-paginate --query="Stacks[].{Name: StackName}"

    if [ $? -ne 0 ]; then
        echo "Stack does not exist, creating it..."

		echo "Script executed from: ${PWD}"

		BASEDIR=$(dirname $0)
		echo "Script location: ${BASEDIR}"
        cd gitlab_runner
		
        callCloudFormation "create-stack" ${time_stamp}

        if [ $? -ne 0 ]; then
            errorExit "Stack creation failed"
        fi

        echo -n "Waiting for stack creation to complete..."
        aws cloudformation wait stack-create-complete --stack-name ${stack_name} --profile ${profile} --region ${region} 2> /dev/null
        if [ $? -ne 0 ]; then
            echo
            echo -n "Stack creation completed with failure..."
        fi

        verifyStackSuccess "CREATE_COMPLETE" "create"
    else
        echo "Stack already exists. Attempting to update it..."
		
	    cd gitlab_runner		        
			
        echo callCloudFormation "update-stack" ${time_stamp} 2> temp.txt

        status=$?
        Temp=`cat temp.txt`
        rm -f temp.txt

        if [ ${status} -ne 0 ]; then
            if echo ${Temp} | grep "No updates are to be performed" > /dev/null ; then
                echo "No updates are needed"
                exit 0
            else
                errorExit "Stack update failed: ${Temp}"
            fi
        fi

        echo "Waiting for stack update to complete..."
        aws cloudformation wait stack-update-complete --stack-name ${stack_name} --profile ${profile} --region ${region} 2> /dev/null
        if [ $? -ne 0 ]; then
            echo "Stack update completed with failure..."
        fi

        verifyStackSuccess "UPDATE_COMPLETE" "update"
    fi
}

script_usage() {
  echo "Usage: $0 <properties-file-path> <region> <aws-profile> <stack-name>"
}

# Read properties for the stack
source $1 || errorExit "Unable to load properties"

region=$2
if [ -z "$region" ]; then
  echo "The region cannot be empty."
  script_usage
  exit 1
fi

profile=$3
if [ -z "$profile" ]; then
  echo "The AWS profile cannot be empty."
  script_usage
  exit 1
fi

stack_name=$4
if [ -z "$stack_name" ]; then
  echo "The CloudFormation stack-name cannot be empty."
  script_usage
  exit 1
fi

if ! command -v jq &> /dev/null; then
  echo "The 'jq' command could not be found, please install it before continuing."
  exit 1
fi

ami_ssm_path="/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
echo "Retrieving last Amazon Linux AMI ID at SSM path '$ami_ssm_path' for region '$region'"
ImageId=$(aws ssm get-parameters --names "$ami_ssm_path" --region "$region" --profile "$profile" | jq '.Parameters[0].Value')
if [ -z "$ImageId" ]; then
  echo "Could not retrieve latest image id with these parameters, cannot continue."
  exit 1
else
  echo "Retrieved AMI ID $ImageId"
fi

gitlab_registration_token_ssm_path="/dev/gitlab-registration-token"
echo "Retrieving Gitlab registration token from SSM path '$gitlab_registration_token_ssm_path'."
RunnerRegistrationTokens=$(aws ssm get-parameters --names "$gitlab_registration_token_ssm_path" --region "$region" --profile "$profile" | jq '.Parameters[0].Value')
if [ -z "$RunnerRegistrationTokens" ]; then
  echo "Could not retrieve Gitlab runner registration token with these parameters, cannot continue."
  exit 1
else
  echo "Successfully retrieved gitlab runner registration token."
fi

echo "Check if '${LambdaS3Bucket}' bucket exists, creating it if not."
if aws s3api head-bucket --bucket ${LambdaS3Bucket} --region $region --profile $profile 2> /dev/null ; then
  echo "Bucket $LambdaS3Bucket exists, nothing to do."
else
  echo "Bucket $LambdaS3Bucket does not exist, creating it..."
  aws s3 mb s3://$LambdaS3Bucket --region $region --profile $profile
  aws s3api put-bucket-policy --bucket $LambdaS3Bucket --policy file://bucket-policy.json --region $region --profile $profile
  aws s3api put-public-access-block --bucket $LambdaS3Bucket --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true" --region $region --profile $profile
fi

echo "Add missing IAM Roles if needed."
role_name="AWSServiceRoleForAutoScaling"
if aws iam get-role --role-name ${role_name} --region $region --profile $profile 2> /dev/null ; then
  echo "Role '${role_name}' exists, nothing to do."
else
  echo "Role '${role_name}' does not exist, creating it..."
  aws iam create-service-linked-role --aws-service-name autoscaling.amazonaws.com --region $region --profile $profile
fi

echo "Deploying Gitlab runner in region: "$region", using AWS profile: "$profile
echo "CloudFormation stack name: "$stack_name

#Suffix for the lambda zip
time_stamp=`date "+%Y%m%d%H%M%S"` 

uploadLifecycleHookToS3
uploadRunnerMonitorToS3
deployStack
